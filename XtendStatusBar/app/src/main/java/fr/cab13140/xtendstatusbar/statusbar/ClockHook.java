package fr.cab13140.xtendstatusbar.statusbar;

import android.app.Activity;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.widget.TextView;

import java.util.Random;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;

/**
 * Hooks for clock.
 * Actually implemented :
 * - Rainbow Clock
 *
 * WIP :
 * - Custom clock color
 *
 */
public class ClockHook extends Activity implements IXposedHookLoadPackage {
    @Override
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        final boolean verbose = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("xposedLoggingVerbosity",0) == 1; // Check Logging verbosity setting
        final boolean rainbowClock = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("rainbowclock",false); // Check if RainbowClock is activated

        // Rainbow clock method hook
        if (rainbowClock) {
            if (verbose)
                XposedBridge.log("Hooked method for Rainbow Clock. Unicorns will find and kill you.");
            findAndHookMethod("com.android.systemui.statusbar.policy.Clock", loadPackageParam.classLoader, "updateClock", new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    TextView clockText = (TextView) param.thisObject;
                    String[] colorRainbow = {"F44336", "E91E63", "9C27B0", "673AB7", "3F51B5", "2196F3", "03A9F4", "00BCD4", "009688", "4CAF50", "8BC34A", "CDDC39", "FFEB3B", "FFC107", "FF9800", "FF5722", "795548", "607D8B"};
                    clockText.setTextColor(Color.parseColor("#" + colorRainbow[new Random().nextInt(colorRainbow.length)]));
                }
            });
        }

    }
}
