package fr.cab13140.xtendstatusbar.ui;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import fr.cab13140.xtendstatusbar.BuildConfig;
import fr.cab13140.xtendstatusbar.R;

public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        assert (getActionBar() != null);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setElevation(16f);
        getActionBar().setSubtitle(R.string.subtitle_about);

        TextView versionText = (TextView) findViewById(R.id.versionText);
        versionText.setText(versionText.getText().toString().concat(" "+ BuildConfig.VERSION_NAME).concat(" (Build "+BuildConfig.VERSION_CODE+")"));
    }
}
