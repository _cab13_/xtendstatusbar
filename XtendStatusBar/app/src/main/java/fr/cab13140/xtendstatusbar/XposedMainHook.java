package fr.cab13140.xtendstatusbar;

import android.app.Activity;
import android.preference.PreferenceManager;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import fr.cab13140.xtendstatusbar.statusbar.ClockHook;

/**
 * Ajouté par danit le 13/05/2016 à 22:42
 */
public class XposedMainHook extends Activity implements IXposedHookLoadPackage {
    @SuppressWarnings("FieldCanBeLocal")
    private final String SYSTEMUI_PACKAGE = "com.android.systemui";

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        boolean verbose = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("xposedLoggingVerbosity",0) == 1;
        boolean quiet = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("xposedLoggingVerbosity",0) == -1;

        if (!quiet)
            XposedBridge.log("XtendStausBar is ready ! Module and hooks loaded !");
        switch (loadPackageParam.packageName){
            case SYSTEMUI_PACKAGE:
                if(verbose)
                    XposedBridge.log("SystemUI loaded: " + loadPackageParam.packageName);
                if(verbose)
                    XposedBridge.log("Launching hook for Clock");
                new ClockHook();
        }

    }
}
