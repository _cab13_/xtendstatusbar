package fr.cab13140.xtendstatusbar.ui;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.v4.app.ShareCompat;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import fr.cab13140.xtendstatusbar.BuildConfig;
import fr.cab13140.xtendstatusbar.R;

/**
 * Ajouté par danit le 10/05/2016 à 14:06
 */
public class MainPreferenceActivity extends PreferenceActivity {

    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.headers_pref, target);
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        return GeneralSettingsFragment.class.getName().equals(fragmentName)
                || LoggingSettingsFragment.class.getName().equals(fragmentName)
				|| StatusbarSettingsFragment.class.getName().equals(fragmentName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        assert getActionBar() != null;
        getActionBar().setSubtitle(R.string.subtitle_main);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()){
            case R.id.about_menu:
                startActivity(new Intent(this,AboutActivity.class)); // Start AboutActivity
                return true;
            case R.id.contribs_menu:
                startActivity(new Intent(this,ContribsActivity.class));
                return true;
            case R.id.feedback_menu:
                ShareCompat.IntentBuilder.from(this) // Start chooser for e-mail
                        .setType("message/rfc822")
                        .addEmailTo("danielouthirion@gmail.com")
                        .setSubject("Feedback: XtendStatusBar v"+ BuildConfig.VERSION_NAME)
                        .setChooserTitle(R.string.feedback_choosed)
                        .startChooser();
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    /**
     * Fragment related to advanced settings
     */
    public static class LoggingSettingsFragment extends PreferenceFragment{
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings_logging);
        }
    }

    /**
     * Fragment related to general settings
     */
    public static class GeneralSettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings_general);
        }
    }
	/**
	 * Fragment related to Statusbar Tweaks.
	 */
	public static class StatusbarSettingsFragment extends PreferenceFragment
	{

		@Override
		public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.settings_statusbar);
		}
		
	}
}
